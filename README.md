# enno.digital

## installation

### Clone code

```
git clone https://gitlab.com/dimichspb/enno.git
```

### Compose the components

```
docker-compose up
```

* nginx is configurated to listen to port 8080 at localhost by default, so if the port is already busy, please change it 
in docker-compose.yml at line 11

if you do not have docker installed on your machine, then follow the next steps:

- Update database settings in "./app/.env"
- Run "composer install" in "./app" folder
- Apply database schema by running "php bin/console doctrine:schema:update --force" in "./app" folder
- Configure your web-service's root to "./app/public"

### Open in browser

```
http://127.0.0.1:8080
```

or use the port and hostname configurated on the previous step