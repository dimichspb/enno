<?php
namespace App\Tests\Form\Type;

use App\Entity\User;
use App\Form\ProfileType;
use Symfony\Component\Form\Test\TypeTestCase;

class ProfileTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = [
            'firstname' => 'test',
            'lastname' => 'test',
            'password' => ['first' => 'test', 'second' => 'test'],
        ];

        $model = new User();

        // $formData will retrieve data from the form submission; pass it as the second argument
        $form = $this->factory->create(ProfileType::class, $model);

        // ...populate $object properties with the data stored in $formData
        $expected = new User();
        $expected->setFirstname('test');
        $expected->setLastname('test');
        $expected->setPassword('test');

        // submit the data to the form directly
        $form->submit($formData);

        // This check ensures there are no transformation failures
        $this->assertTrue($form->isSynchronized());

        // check that $formData was modified as expected when the form was submitted
        $this->assertEquals($expected, $model);
    }

    public function testSubmitValidDataWithoutPassword()
    {
        $formData = [
            'firstname' => 'test',
            'lastname' => 'test',
            'password' => ['first' => '', 'second' => ''],
        ];

        $model = new User();

        // $formData will retrieve data from the form submission; pass it as the second argument
        $form = $this->factory->create(ProfileType::class, $model);

        // ...populate $object properties with the data stored in $formData
        $expected = new User();
        $expected->setFirstname('test');
        $expected->setLastname('test');
        $expected->setPassword('');

        // submit the data to the form directly
        $form->submit($formData);

        // This check ensures there are no transformation failures
        $this->assertTrue($form->isSynchronized());

        // check that $formData was modified as expected when the form was submitted
        $this->assertEquals($expected, $model);
    }
}