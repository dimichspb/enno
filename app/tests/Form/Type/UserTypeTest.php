<?php
namespace App\Tests\Form\Type;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Form\Test\TypeTestCase;

class UserTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = [
            'username' => 'test',
            'firstname' => 'test',
            'lastname' => 'test',
            'password' => ['first' => 'test', 'second' => 'test'],
        ];

        $model = new User();

        // $formData will retrieve data from the form submission; pass it as the second argument
        $form = $this->factory->create(UserType::class, $model);

        // ...populate $object properties with the data stored in $formData
        $expected = new User();
        $expected->setUsername('test');
        $expected->setFirstname('test');
        $expected->setLastname('test');
        $expected->setPassword('test');

        // submit the data to the form directly
        $form->submit($formData);

        // This check ensures there are no transformation failures
        $this->assertTrue($form->isSynchronized());

        // check that $formData was modified as expected when the form was submitted
        $this->assertEquals($expected, $model);
    }
}