<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ListController
 * @package App\Controller
 */
class ListController extends AbstractController
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * ListController constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/", name="list")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $users = $this->repository->findAll();

        return $this->render('list/index.html.twig', [
            'users' => $users,
        ]);
    }
}
